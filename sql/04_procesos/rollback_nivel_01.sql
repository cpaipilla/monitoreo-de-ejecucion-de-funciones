/* A partir de backup sql en tabla */

CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_rollback_nivel1()
 RETURNS int8
 LANGUAGE plpgsql
AS $function$
	DECLARE	
		fn RECORD;
		ct integer := 0;
	BEGIN
		FOR fn IN SELECT sql_anterior FROM soporte_dba.auditoria_funcion LOOP
			ct := ct + 1;
			EXECUTE fn.sql_anterior;
		END LOOP;
		RETURN ct;			
	END;
$function$
;