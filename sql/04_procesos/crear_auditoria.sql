
--DROP FUNCTION soporte_dba.fn_crear_auditoria_funcion(_funcion varchar, _inicio timestamp with time zone , _fin timestamp with time zone );
CREATE OR REPLACE FUNCTION soporte_dba.fn_crear_auditoria_funcion(_funcion varchar, _inicio timestamp with time zone DEFAULT NULL, _fin timestamp with time zone DEFAULT NULL)
 RETURNS TABLE (
	_funcion_id integer,
	_nombre_funcion varchar,
	_inicio_auditoria timestamp with time zone,
	_fin_auditoria timestamp with time zone,
	_sql_anterior varchar,
	_sql_nuevo varchar
)
 LANGUAGE plpgsql
AS $function$
	DECLARE	
	BEGIN
		RETURN QUERY
			INSERT INTO soporte_dba.auditoria_funcion(funcion, inicio_auditoria, fin_auditoria, incorporar_sql) 
				VALUES (_funcion, coalesce(_inicio, clock_timestamp()), _fin , TRUE)
			ON conflict ON CONSTRAINT uq_auditoria_funcion_funcion
			DO UPDATE SET 
--				 col = EXCLUDED.col,
				fecha_update = clock_timestamp()
			RETURNING id,
					funcion,
					inicio_auditoria,
					fin_auditoria,
					sql_anterior,
					sql_nuevo 
		;
	END;	
$function$
; 
