
-- DROP FUNCTION soporte_dba.fn_auditoria_funcion_inicio_ejecucion(_sql varchar, _stack varchar);
CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_inicio_ejecucion(_auditoria_funcion varchar, _sql varchar, _stack varchar, _pid integer, _params json DEFAULT null) RETURNS TABLE (ejecucion_id int8, inicio_ejecucion_out timestamp with time zone)
 LANGUAGE plpgsql SECURITY DEFINER
AS $function$
	DECLARE
--		_conn json := (select json_agg(c.*) from pg_stat_get_activity( _pid ) c);
		_conn json := (select json_agg(c.*) from pg_stat_activity c where c.pid=_pid);
	BEGIN 
	RETURN QUERY
		select *
		from dblink( soporte_dba.fn_get_parametros_conexion_externa(), 
			'select * from soporte_dba.fn_auditoria_funcion_inicio_ejecucion_externa('|| quote_literal(_auditoria_funcion) ||','|| quote_literal(_sql) ||','|| quote_literal(_stack)||','|| coalesce(quote_literal(_conn),'null') ||','|| coalesce(quote_literal(_params),'null')  ||');' -- validar funcion format o variable con asignacion de texto
		) externo (ext_ejecucion_id int8, ext_inicio_ejecucion_out timestamp with time zone)
		;
	END;	
$function$
;

-- DROP FUNCTION soporte_dba.fn_auditoria_funcion_error_ejecucion( int8, varchar, varchar, text);
CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_error_ejecucion(_ejecucion_id int8, _sqlerrm varchar, _sqlstate varchar, _contexto text) RETURNS TABLE (ejecucion_id int8, error_ejecucion_out timestamp with time zone)
 LANGUAGE plpgsql SECURITY DEFINER
AS $function$
	DECLARE
	BEGIN 
	RETURN QUERY
		select *
		from dblink( soporte_dba.fn_get_parametros_conexion_externa(), 
			'select * from soporte_dba.fn_auditoria_funcion_error_ejecucion_externa('|| _ejecucion_id ||','|| quote_literal(_sqlerrm) ||','|| quote_literal(_sqlstate)||','|| quote_literal(_contexto) ||');' -- validar funcion format o variable con asignacion de texto
		) externo (ext_ejecucion_id int8, ext_error_ejecucion_out timestamp with time zone)
		;
	END;	
$function$
;

-- DROP FUNCTION soporte_dba.fn_auditoria_funcion_final_ejecucion( int8);
CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_final_ejecucion(_ejecucion_id int8) RETURNS TABLE (ejecucion_id int8, fin_ejecucion_out timestamp with time zone)
 LANGUAGE plpgsql SECURITY DEFINER
AS $function$
	DECLARE
	BEGIN 
	RETURN QUERY
		select *
		from dblink( soporte_dba.fn_get_parametros_conexion_externa(), 
			'select * from soporte_dba.fn_auditoria_funcion_final_ejecucion_externa('|| _ejecucion_id ||');' -- validar funcion format o variable con asignacion de texto
		) externo (ext_ejecucion_id int8, ext_fin_ejecucion_out timestamp with time zone)
		;
	END;	
$function$
;
