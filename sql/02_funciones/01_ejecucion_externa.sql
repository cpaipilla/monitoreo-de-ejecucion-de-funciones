
-- DROP FUNCTION soporte_dba.fn_auditoria_funcion_inicio_ejecucion_externo(_sql varchar, _stack varchar);
CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_inicio_ejecucion_externa(_function varchar, _sql varchar, _stack varchar, _conn json DEFAULT null, _params json DEFAULT null) RETURNS TABLE (ejecucion_id int8, inicio_ejecucion_out timestamp with time zone)
 LANGUAGE plpgsql SECURITY DEFINER
AS $function$
	DECLARE
	BEGIN 
	
	INSERT INTO soporte_dba.auditoria_funcion(funcion, inicio_auditoria, incorporar_sql) 
				VALUES (_function, clock_timestamp(), FALSE)
			ON conflict ON CONSTRAINT uq_auditoria_funcion_funcion
			DO NOTHING
	;
	
	RETURN QUERY
		WITH get_configuracion_funcion AS (
			SELECT  f.id, f.funcion, f.inicio_auditoria, f.fin_auditoria
			FROM soporte_dba.auditoria_funcion f
			WHERE f.funcion = _function
		)
		INSERT INTO soporte_dba.auditoria_ejecucion(
				auditoria_funcion_id,
				str_ejecucion,
				str_query,
				inicio_ejecucion,
				parametros,
				conexion
			)
			SELECT af.id as auditoria_funcion_id,
				_stack as str_ejecucion, 
				_sql as str_query, 
				clock_timestamp() as inicio_ejecucion,
				_params as parametros,
				_conn as conexion
			FROM get_configuracion_funcion af
			WHERE af.funcion = _function
				AND ( clock_timestamp() BETWEEN af.inicio_auditoria AND coalesce( af.fin_auditoria, clock_timestamp() ) )
		RETURNING id, inicio_ejecucion;
	END;	
$function$
;

-- DROP FUNCTION soporte_dba.fn_auditoria_funcion_error_ejecucion_externa( int8, varchar, varchar, text);
CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_error_ejecucion_externa(_ejecucion_id int8, _sqlerrm varchar, _sqlstate varchar, _contexto text) RETURNS TABLE (ejecucion_id int8, error_ejecucion_out timestamp with time zone)
 LANGUAGE plpgsql SECURITY DEFINER
AS $function$
	DECLARE
	BEGIN 
	RETURN QUERY
		UPDATE soporte_dba.auditoria_ejecucion
			SET error = true,
				str_sqlerrm = $2,
				str_sqlstate = $3,
				str_context = $4,
				fin_ejecucion = clock_timestamp(),
				tiempo_ejecucion = age( clock_timestamp(), inicio_ejecucion)
		WHERE id = $1
		RETURNING id, clock_timestamp();
	END;	
$function$
;

-- DROP FUNCTION soporte_dba.fn_auditoria_funcion_final_ejecucion_externa( int8);
CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_final_ejecucion_externa(_ejecucion_id int8) RETURNS TABLE (ejecucion_id int8, fin_ejecucion_out timestamp with time zone)
 LANGUAGE plpgsql SECURITY DEFINER
AS $function$
	DECLARE
	BEGIN 
	RETURN QUERY
		UPDATE soporte_dba.auditoria_ejecucion
			SET fin_ejecucion = clock_timestamp(),
				tiempo_ejecucion = age( clock_timestamp(), inicio_ejecucion)
		WHERE id = $1
		RETURNING id, fin_ejecucion;
	END;	
$function$
;

--drop function if exists soporte_dba.fn_get_parametros_conexion_externa(_ip_server inet, _port_server integer, _db_server name , _user_server name ); 
CREATE OR REPLACE FUNCTION soporte_dba.fn_get_parametros_conexion_externa(_ip_server inet DEFAULT NULL, _port_server integer DEFAULT NULL, _db_server name DEFAULT NULL, _user_server name DEFAULT NULL) RETURNS varchar 
 LANGUAGE plpgsql 
AS $function$
	BEGIN
		RETURN 'host='|| host( coalesce( _ip_server, CASE WHEN pg_is_in_recovery() THEN (select sender_host::inet from pg_stat_wal_receiver limit 1) ELSE inet_server_addr() END ) ) 
			||' port='|| coalesce( _port_server, inet_server_port() ) 
			||' dbname='|| coalesce( _db_server, current_database() ) 
			||' user='|| coalesce(_user_server, 'postgres') --> .pgpass
		; 
	END;
$function$;
