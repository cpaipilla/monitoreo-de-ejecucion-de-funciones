/* PRUEBA DBA */

DROP TABLE IF EXISTS soporte_dba.prueba_dba;
CREATE TABLE soporte_dba.prueba_dba (id smallserial,valor numeric, fecha timestamp  with time zone);

-- DROP FUNCTION soporte_dba.fn_plantilla_ejecutar(numeric,numeric, integer);
CREATE OR REPLACE FUNCTION soporte_dba.fn_plantilla_ejecutar(_valor numeric, _div numeric DEFAULT 1.0, _seg integer DEFAULT 10)
 RETURNS TABLE (prueba_id int2, otro timestamp with time zone)
 LANGUAGE plpgsql
AS $function$
	DECLARE	
		var_ini int2 := 1::int2;
	BEGIN
		raise notice 'var_ini = %', var_ini;
		PERFORM pg_sleep(_seg);
	------------------------------------------------------------------------------------------
		RETURN QUERY 
			INSERT INTO soporte_dba.prueba_dba (valor,fecha) 
				SELECT (_valor / _div), clock_timestamp()	
			RETURNING id, fecha
		;	
	------------------------------------------------------------------------------------------			
	END;
$function$
;
/* CONSULTAS DBA */
/*
	SELECT * from soporte_dba.fn_crear_auditoria_funcion('soporte_dba.fn_plantilla_ejecutar(numeric, numeric, integer)'); -- Incorpora codigo sql a la funcion
	
	SELECT * FROM soporte_dba.auditoria_funcion;
	SELECT * FROM soporte_dba.auditoria_ejecucion;

	SELECT * FROM soporte_dba.fn_plantilla_ejecutar(10, 2); -- Registra primera ejecución, sin problemas.
	SELECT * FROM soporte_dba.fn_plantilla_ejecutar(20, 0); -- Registra segunda ejecución, con problemas.

*/


-------------------- LAS SIGUIENTES SON CASOS PRESENTADOS SEGUN PARAMETROS DE ENTRADA Y TIPOS DE RETURN

-- DROP FUNCTION soporte_dba.fn_plantilla_ejecutar2(numeric,numeric, integer);
CREATE OR REPLACE FUNCTION soporte_dba.fn_plantilla_ejecutar2(_valor numeric, _div numeric DEFAULT 1.0, _seg integer DEFAULT 10)
 RETURNS int8
 LANGUAGE plpgsql
AS $function$
	DECLARE	
		var_ini int2 := 1::int2;
	BEGIN
		raise notice 'var_ini = %', var_ini;
		PERFORM pg_sleep(_seg);
	------------------------------------------------------------------------------------------
		RETURN  (_valor / _div)
		;	
	------------------------------------------------------------------------------------------			
	END;
$function$
;

CREATE OR REPLACE FUNCTION soporte_dba.fn_plantilla_ejecutar_json("_ValoR" json)
 RETURNS int8
 LANGUAGE plpgsql
AS $function$
	BEGIN	
		PERFORM pg_sleep(2);
	------------------------------------------------------------------------------------------
		RETURN  -1
		;	
	------------------------------------------------------------------------------------------			
	END;
$function$
;


CREATE OR REPLACE FUNCTION soporte_dba.fn_plantilla_ejecutar_sin_param()
 RETURNS int8
 LANGUAGE plpgsql
AS $function$
	BEGIN	
		PERFORM pg_sleep(2);
	------------------------------------------------------------------------------------------
		RETURN  0
		;	
	------------------------------------------------------------------------------------------			
	END;
$function$
;

