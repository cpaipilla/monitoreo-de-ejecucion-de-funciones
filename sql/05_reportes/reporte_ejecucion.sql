
-- drop function soporte_dba.fn_auditoria_funcion_reporte_ejecuciones(  varchar  );
CREATE OR REPLACE FUNCTION soporte_dba.fn_auditoria_funcion_reporte_ejecuciones( _funcion varchar DEFAULT NULL)
 RETURNS TABLE (numero_ejecuciones int8, tiempo_maximo_ejecucion interval, tiempo_promedio_ejecucion interval, tiempo_minimo_ejecucion interval,funcion_id int4, funcion varchar, ultima_ejecucion timestamp)
 LANGUAGE plpgsql
AS $function$
	BEGIN
		RETURN QUERY
			select count(1) as numero_ejecuciones,
				max( e.tiempo_ejecucion ) as _tiempo_maximo,
				avg( e.tiempo_ejecucion ) as _tiempo_promedio,
				min( e.tiempo_ejecucion ) as _tiempo_minimo,
				f.id, f.funcion, max(e.fecha_insert) as ultima_ejecucion
			from soporte_dba.auditoria_funcion f 
				inner join soporte_dba.auditoria_ejecucion e 
					on f.id = e.auditoria_funcion_id 
			where  e.fin_ejecucion is not null
				and ( _funcion is null OR f.funcion=_funcion)
			group by f.id, f.funcion--, f.ultima_ejecucion
			--order by _columna_orden desc
		;
	END;
$function$
;

/*
-- CONSULTAS QUE MUESTRAN LAS QUE ESTAN EN EJECUCIÓN O QUE FUERON CANCELADAS
Tal vez se puedan diferenciar usando pg_stat_activity de las que aun tiene conexiones activas.

select * from soporte_dba.auditoria_ejecucion where inicio_ejecucion >='2020-10-20' and fin_ejecucion is null order by inicio_ejecucion desc

with funciones as (
	select * 
	from soporte_dba.auditoria_funcion 
	where (
		funcion like 'intercambio_contratacion.%'
		or funcion like 'contratacion_n2.%'
	)
)
select e.inicio_ejecucion, e.fin_ejecucion, e.str_query, f.funcion, e.parametros, e.conexion
from funciones f 
	inner join soporte_dba.auditoria_ejecucion e on e.auditoria_funcion_id = f.id  
where inicio_ejecucion >='2020-10-20' and fin_ejecucion is null 
order by e.fecha_insert

*/