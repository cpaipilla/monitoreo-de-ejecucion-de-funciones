/* EXTENSIONES */
CREATE EXTENSION IF NOT EXISTS dblink;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
/* ESQUEMA */
-- 
drop SCHEMA  IF EXISTS soporte_dba cascade;
CREATE SCHEMA soporte_dba;


GRANT USAGE ON SCHEMA soporte_dba TO gp_positiva_ejecutor;

GRANT USAGE ON SCHEMA soporte_dba TO gp_positiva_escritura;

GRANT USAGE ON SCHEMA soporte_dba TO gp_positiva_lectura;


GRANT ALL ON SCHEMA soporte_dba TO postgres;

ALTER DEFAULT PRIVILEGES IN SCHEMA soporte_dba
GRANT SELECT ON TABLES TO gp_positiva_ejecutor;

ALTER DEFAULT PRIVILEGES IN SCHEMA soporte_dba
GRANT SELECT, UPDATE, INSERT, TRIGGER, DELETE ON TABLES TO gp_positiva_escritura;

ALTER DEFAULT PRIVILEGES IN SCHEMA soporte_dba
GRANT SELECT ON TABLES TO gp_positiva_lectura;
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
/* TABLAS */


DROP TABLE IF EXISTS soporte_dba.auditoria_funcion;
CREATE TABLE soporte_dba.auditoria_funcion (
	id serial,
	funcion varchar,
	inicio_auditoria timestamp with time zone DEFAULT clock_timestamp(),
	fin_auditoria timestamp with time zone DEFAULT NULL, -- Para dejar de registrar LOG de auditoria. por el momento agregar mensaje en la función donde se recomiende retirar manualmente el codigo de auditoria.
--	ultima_ejecucion timestamp with time zone DEFAULT NULL, -- 
	vigencia_registros interval DEFAULT '3 months'::interval, --> Realizar borrado automatico de registros. (renombrar ultima_ejecucion, actualizar a nulo, y cambiar tipo de dato.)
	incorporar_sql boolean DEFAULT TRUE,
	sql_anterior varchar DEFAULT NULL,
	sql_nuevo varchar DEFAULT NULL,
	fecha_insert timestamp DEFAULT clock_timestamp(),
	fecha_update timestamp,
	CONSTRAINT pk_auditoria_funcion PRIMARY KEY (id),
	CONSTRAINT uq_auditoria_funcion_funcion UNIQUE (funcion)
);

DROP TABLE IF EXISTS soporte_dba.auditoria_ejecucion;
CREATE TABLE soporte_dba.auditoria_ejecucion ( 
	id bigserial,
	auditoria_funcion_id integer NOT NULL,
	str_ejecucion varchar,
	str_query varchar,
	parametros json,
	inicio_ejecucion timestamp with time zone,
	fin_ejecucion timestamp with time zone,
	tiempo_ejecucion interval,
	conexion json,
	error boolean DEFAULT false,
	str_sqlerrm varchar, 
	str_sqlstate varchar,
	str_context varchar,
	fecha_insert timestamp DEFAULT clock_timestamp(),
	CONSTRAINT pk_auditoria_ejecucion PRIMARY KEY (id,auditoria_funcion_id),
	CONSTRAINT fk_auditoria_ejecucion_autiroria_funcion_id FOREIGN KEY(auditoria_funcion_id) REFERENCES soporte_dba.auditoria_funcion(id)
) PARTITION BY HASH (auditoria_funcion_id)
WITH (
    OIDS = FALSE
)
;

CREATE INDEX ix_auditoria_ejecucion_auditoria_funcion_id ON soporte_dba.auditoria_ejecucion(auditoria_funcion_id);
CREATE INDEX ix_auditoria_ejecucion_inicio_ejecucion ON soporte_dba.auditoria_ejecucion(inicio_ejecucion);
CREATE INDEX ix_auditoria_ejecucion_fin_ejecucion ON soporte_dba.auditoria_ejecucion(fin_ejecucion);
CREATE INDEX ix_auditoria_ejecucion_error ON soporte_dba.auditoria_ejecucion(error);

create table soporte_dba.auditoria_ejecucion_p0 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 0) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p1 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 1) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p2 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 2) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p3 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 3) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p4 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 4) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p5 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 5) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p6 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 6) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p7 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 7) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p8 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 8) WITH (autovacuum_enabled = FALSE );
create table soporte_dba.auditoria_ejecucion_p9 partition of soporte_dba.auditoria_ejecucion FOR VALUES WITH (MODULUS 10, REMAINDER 9) WITH (autovacuum_enabled = FALSE );
